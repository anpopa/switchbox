/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define SBOX_TYPE_WINDOW (sbox_window_get_type ())

G_DECLARE_FINAL_TYPE (SBoxWindow, sbox_window, SBOX, WINDOW, AdwApplicationWindow)

void sbox_window_reload_entries (SBoxWindow *win);
void sbox_window_set_switch_state (SBoxWindow *win, gulong id, gboolean state);

G_END_DECLS
