/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sbox-settings.h"
#include "sbox-config.h"
#include "sbox-delegate.h"

enum {
  COLUMN_ENTRY_NAME,
  COLUMN_ENTRY_DESCRIPTION,
  COLUMN_ENTRY_TOPIC,
  COLUMN_ENTRY_PAYLOAD_ON,
  COLUMN_ENTRY_PAYLOAD_OFF,
  NUM_ENTRY_COLUMNS
};

struct _SBoxSettings {
  GtkWindow parent_instance;

  /* Window widgets */
  GtkHeaderBar *header_bar;
  GtkEntry *address_entry;
  GtkEntry *port_entry;
  GtkListStore *switch_liststore;
  GtkTreeView *treeview;

  GtkCellRendererText *name_cell;
  GtkCellRendererText *desc_cell;
  GtkCellRendererText *topic_cell;
  GtkCellRendererText *pldon_cell;
  GtkCellRendererText *pldoff_cell;

  GtkButton *add_button;
  GtkButton *rem_button;
};

G_DEFINE_TYPE (SBoxSettings, sbox_settings, GTK_TYPE_WINDOW)

static void
sbox_settings_class_init (SBoxSettingsClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/ro/fxdata/switchbox/gui/sbox-settings.ui");

  /* Grab main menu widgets */
  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, header_bar);
  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, address_entry);
  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, port_entry);
  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, switch_liststore);
  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, treeview);

  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, name_cell);
  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, desc_cell);
  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, topic_cell);
  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, pldon_cell);
  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, pldoff_cell);

  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, add_button);
  gtk_widget_class_bind_template_child (widget_class, SBoxSettings, rem_button);
}

static void
update_database_entries (SBoxSettings *self)
{
  GtkTreeIter iter;
  gboolean valid;

  g_assert (self);

  valid = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (self->switch_liststore), &iter);
  while (valid)
    {
      g_autofree gchar *name = NULL;
      g_autofree gchar *desc = NULL;
      g_autofree gchar *topic = NULL;
      g_autofree gchar *pldon = NULL;
      g_autofree gchar *pldoff = NULL;
      SBoxSWEntry *entry = NULL;

      gtk_tree_model_get (GTK_TREE_MODEL (self->switch_liststore),
                          &iter,
                          COLUMN_ENTRY_NAME,
                          &name,
                          COLUMN_ENTRY_DESCRIPTION,
                          &desc,
                          COLUMN_ENTRY_TOPIC,
                          &topic,
                          COLUMN_ENTRY_PAYLOAD_ON,
                          &pldon,
                          COLUMN_ENTRY_PAYLOAD_OFF,
                          &pldoff,
                          -1);

      entry = sbox_swentry_new ();

      sbox_swentry_set_name (entry, name);
      sbox_swentry_set_description (entry, desc);
      sbox_swentry_set_topic (entry, topic);  /* will build the id */
      sbox_swentry_set_payload_on (entry, pldon);
      sbox_swentry_set_payload_off (entry, pldoff);

      {
        ExecutorActionPayload pld = { .entry = entry };
        sbox_executor_push_event (sbox_delegate_get_executor (), EXECUTOR_ADD_SWENTRY, pld);
      }

      valid = gtk_tree_model_iter_next (GTK_TREE_MODEL (self->switch_liststore), &iter);
    }
}

static gboolean
on_settings_window_hide_cb (GtkWidget *widget, GdkEvent *event, gpointer data)
{
  SBoxSettings *self = (SBoxSettings *)widget;

  g_autoptr (SBoxDatabase) db = sbox_delegate_get_database ();
  GtkEntryBuffer *addrbuf = gtk_entry_get_buffer (self->address_entry);
  GtkEntryBuffer *portbuf = gtk_entry_get_buffer (self->port_entry);

  SBOX_UNUSED (event);
  SBOX_UNUSED (data);

  g_debug ("On hide signal");
  sbox_database_set_connection_host_addr (db, gtk_entry_buffer_get_text (addrbuf), NULL);
  sbox_database_set_connection_host_port (
    db, g_ascii_strtoll (gtk_entry_buffer_get_text (portbuf), NULL, 10), NULL);

  /* Update database with new entry values */
  update_database_entries (self);

  /* Clean the liststore */
  gtk_list_store_clear (self->switch_liststore);

  return TRUE;
}

static void
add_listore_entry_foreach_cb (gpointer database, gpointer swentry, gpointer arg1, gpointer arg2)
{
  SBoxSWEntry *entry = (SBoxSWEntry *)swentry;
  SBoxSettings *self = (SBoxSettings *)arg1;
  GtkTreeIter *iter = (GtkTreeIter *)arg2;
  GtkTreeViewColumn *column;

  g_assert (self);
  g_assert (entry);
  SBOX_UNUSED (database);

  gtk_list_store_append (self->switch_liststore, iter);
  gtk_list_store_set (self->switch_liststore,
                      iter,
                      COLUMN_ENTRY_NAME,
                      sbox_swentry_get_name (entry),
                      COLUMN_ENTRY_DESCRIPTION,
                      sbox_swentry_get_description (entry),
                      COLUMN_ENTRY_TOPIC,
                      sbox_swentry_get_topic (entry),
                      COLUMN_ENTRY_PAYLOAD_ON,
                      sbox_swentry_get_payload_on (entry),
                      COLUMN_ENTRY_PAYLOAD_OFF,
                      sbox_swentry_get_payload_off (entry),
                      -1);

  for (int i = 0; i < NUM_ENTRY_COLUMNS; i++)
    {
      column = gtk_tree_view_get_column (self->treeview, i);
      g_object_set_data (G_OBJECT (column), "column", GINT_TO_POINTER (i));
    }
}

static void
add_liststore_entries (SBoxSettings *self)
{
  GtkTreeIter iter;

  g_assert (self);

  sbox_database_call_foreach_entry (
    sbox_delegate_get_database (), add_listore_entry_foreach_cb, self, &iter, NULL);
}

static gboolean
on_settings_window_show_cb (GtkWidget *widget, GdkEvent *event, gpointer data)
{
  SBoxSettings *self = (SBoxSettings *)widget;

  g_autoptr (SBoxDatabase) db = sbox_delegate_get_database ();
  g_autofree gchar *addr = sbox_database_get_connection_host_addr (db, NULL);
  glong port = sbox_database_get_connection_host_port (db, NULL);
  GtkEntryBuffer *addrbuf = gtk_entry_get_buffer (self->address_entry);
  GtkEntryBuffer *portbuf = gtk_entry_get_buffer (self->port_entry);
  g_autofree gchar *portstr = g_strdup_printf ("%ld", port);

  SBOX_UNUSED (event);
  SBOX_UNUSED (data);

  g_debug ("On show signal");
  gtk_entry_buffer_set_text (addrbuf, addr, -1);
  gtk_entry_buffer_set_text (portbuf, portstr, -1);

  /* Populate table */
  add_liststore_entries (self);

  return TRUE;
}

static void
on_settings_cell_edited_cb (GtkCellRendererText *cell,
                            const char *path_string,
                            const char *new_text,
                            gpointer data)
{
  SBoxSettings *self = (SBoxSettings *)data;
  GtkTreePath *path = gtk_tree_path_new_from_string (path_string);
  GtkTreeIter iter;

  int column = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (cell), "column"));

  g_debug ("On edited path='%s' new_text='%s' column=%d", path_string, new_text, column);

  gtk_tree_model_get_iter (GTK_TREE_MODEL (self->switch_liststore), &iter, path);
  gtk_list_store_set (self->switch_liststore, &iter, column, new_text, -1);

  gtk_tree_path_free (path);
}

static void
on_add_item_button_clicked_cb (GtkWidget *button, gpointer data)
{
  SBoxSettings *self = (SBoxSettings *)data;
  GtkTreeIter current, iter;
  GtkTreePath *path;
  GtkTreeViewColumn *column;

  SBOX_UNUSED (button);

  /* Insert a new row below the current one */
  gtk_tree_view_get_cursor (self->treeview, &path, NULL);

  if (path)
    {
      gtk_tree_model_get_iter (GTK_TREE_MODEL (self->switch_liststore), &current, path);
      gtk_tree_path_free (path);
      gtk_list_store_insert_after (self->switch_liststore, &iter, &current);
    }
  else
    gtk_list_store_insert (self->switch_liststore, &iter, -1);

  /* Set the data for the new row */
  gtk_list_store_set (self->switch_liststore,
                      &iter,
                      COLUMN_ENTRY_NAME,
                      "NewSwitch",
                      COLUMN_ENTRY_DESCRIPTION,
                      "Dummy switch entry",
                      COLUMN_ENTRY_TOPIC,
                      "/dummy/switch/topic",
                      COLUMN_ENTRY_PAYLOAD_ON,
                      "ON",
                      COLUMN_ENTRY_PAYLOAD_OFF,
                      "OFF",
                      -1);

  /* Move focus to the new row */
  path = gtk_tree_model_get_path (GTK_TREE_MODEL (self->switch_liststore), &iter);
  column = gtk_tree_view_get_column (self->treeview, 0);
  gtk_tree_view_set_cursor (self->treeview, path, column, FALSE);

  gtk_tree_path_free (path);
}

static void
on_remove_item_button_clicked_cb (GtkWidget *button, gpointer data)
{
  SBoxSettings *self = (SBoxSettings *)data;
  GtkTreeIter iter;
  GtkTreeSelection *selection = gtk_tree_view_get_selection (self->treeview);

  SBOX_UNUSED (button);

  if (gtk_tree_selection_get_selected (selection, NULL, &iter))
    {
      SBoxSWEntry *entry = sbox_swentry_new ();
      g_autofree gchar *topic = NULL;

      gtk_tree_model_get (GTK_TREE_MODEL (self->switch_liststore), &iter, 2, &topic, -1);

      sbox_swentry_set_topic (entry, topic);  /* will build the id */

      {
        ExecutorActionPayload pld = { .entry = entry };
        sbox_executor_push_event (sbox_delegate_get_executor (), EXECUTOR_REM_SWENTRY, pld);
      }

      gtk_list_store_remove (self->switch_liststore, &iter);
    }
}

static void
sbox_settings_init (SBoxSettings *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  g_signal_connect (G_OBJECT (self), "hide", G_CALLBACK (on_settings_window_hide_cb), NULL);
  g_signal_connect (G_OBJECT (self), "show", G_CALLBACK (on_settings_window_show_cb), NULL);

  g_signal_connect (
    G_OBJECT (self->name_cell), "edited", G_CALLBACK (on_settings_cell_edited_cb), self);
  g_signal_connect (
    G_OBJECT (self->desc_cell), "edited", G_CALLBACK (on_settings_cell_edited_cb), self);
  g_signal_connect (
    G_OBJECT (self->topic_cell), "edited", G_CALLBACK (on_settings_cell_edited_cb), self);
  g_signal_connect (
    G_OBJECT (self->pldon_cell), "edited", G_CALLBACK (on_settings_cell_edited_cb), self);
  g_signal_connect (
    G_OBJECT (self->pldoff_cell), "edited", G_CALLBACK (on_settings_cell_edited_cb), self);

  g_signal_connect (
    G_OBJECT (self->add_button), "clicked", G_CALLBACK (on_add_item_button_clicked_cb), self);
  g_signal_connect (
    G_OBJECT (self->rem_button), "clicked", G_CALLBACK (on_remove_item_button_clicked_cb), self);

  g_object_set_data (G_OBJECT (self->name_cell), "column", GINT_TO_POINTER (COLUMN_ENTRY_NAME));
  g_object_set_data (
    G_OBJECT (self->desc_cell), "column", GINT_TO_POINTER (COLUMN_ENTRY_DESCRIPTION));
  g_object_set_data (G_OBJECT (self->topic_cell), "column", GINT_TO_POINTER (COLUMN_ENTRY_TOPIC));
  g_object_set_data (
    G_OBJECT (self->pldon_cell), "column", GINT_TO_POINTER (COLUMN_ENTRY_PAYLOAD_ON));
  g_object_set_data (
    G_OBJECT (self->pldoff_cell), "column", GINT_TO_POINTER (COLUMN_ENTRY_PAYLOAD_OFF));
}
