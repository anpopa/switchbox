/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sbox-switchrow.h"
#include "sbox-config.h"
#include "sbox-delegate.h"

struct _SBoxSwitchRow {
  GtkListBoxRow parent_instance;

  gulong id;

  /* Window widgets */
  GtkSwitch *entry_switch;
  GtkLabel *name_label;
  GtkLabel *desc_label;
  GtkLabel *state_label;
};

G_DEFINE_TYPE (SBoxSwitchRow, sbox_switchrow, GTK_TYPE_LIST_BOX_ROW)

static void
sbox_switchrow_class_init (SBoxSwitchRowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/ro/fxdata/switchbox/gui/sbox-switchrow.ui");

  /* Grab main menu widgets */
  gtk_widget_class_bind_template_child (widget_class, SBoxSwitchRow, entry_switch);
  gtk_widget_class_bind_template_child (widget_class, SBoxSwitchRow, name_label);
  gtk_widget_class_bind_template_child (widget_class, SBoxSwitchRow, desc_label);
  gtk_widget_class_bind_template_child (widget_class, SBoxSwitchRow, state_label);
}

static gboolean
do_reset_switch (gpointer user_data)
{
  SBoxSwitchRow *box = (SBoxSwitchRow *)user_data;

  if (!sbox_connection_is_connected (sbox_delegate_get_connection ()))
    {
      gtk_switch_set_active (box->entry_switch, FALSE);
      sbox_switchrow_set_state (box, FALSE);
    }

  return G_SOURCE_REMOVE;
}

static gboolean
do_entry_switch_activated_cb (GtkSwitch *self, gboolean state, gpointer user_data)
{
  g_autoptr (SBoxExecutor) executor = sbox_delegate_get_executor ();
  SBoxSwitchRow *box = (SBoxSwitchRow *)user_data;

  g_assert (box);

  if (sbox_connection_is_connected (sbox_delegate_get_connection ()))
    {
      SBoxSWEntry *entry = sbox_swentry_new ();
      ExecutorActionPayload pld = { .entry = entry };

      sbox_swentry_set_id (entry, box->id);
      sbox_swentry_set_switch_state (entry, state);
      sbox_swentry_set_widget (entry, g_object_ref (self));

      sbox_switchrow_set_state (box, state);

      sbox_executor_push_event (executor, EXECUTOR_SET_SWENTRY_SWITCH_STATE, pld);
    }
  else
    {
      sbox_switchrow_set_state (box, state);
      g_timeout_add_seconds_full (G_PRIORITY_DEFAULT, 1, do_reset_switch, box, NULL);
    }

  return FALSE;
}

static void
sbox_switchrow_init (SBoxSwitchRow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  g_signal_connect (
    G_OBJECT (self->entry_switch), "state-set", G_CALLBACK (do_entry_switch_activated_cb), self);
}

void
sbox_switchrow_set_id (SBoxSwitchRow *box, gulong id)
{
  g_assert (box);
  box->id = id;
}

gulong
sbox_switchrow_get_id (SBoxSwitchRow *box)
{
  g_assert (box);
  return box->id;
}

void
sbox_switchrow_set_name (SBoxSwitchRow *box, const gchar *name)
{
  g_assert (box);
  g_assert (name);
  gtk_label_set_label (box->name_label, name);
}

void
sbox_switchrow_set_desc (SBoxSwitchRow *box, const gchar *desc)
{
  g_assert (box);
  g_assert (desc);
  gtk_label_set_label (box->desc_label, desc);
}

void
sbox_switchrow_set_state (SBoxSwitchRow *box, gboolean state)
{
  g_assert (box);

  if (state)
    gtk_label_set_label (box->state_label, "ON");
  else
    gtk_label_set_label (box->state_label, "OFF");
}

void
sbox_switchrow_set_switch (SBoxSwitchRow *box, gboolean state)
{
  g_assert (box);
  gtk_switch_set_active (box->entry_switch, state);
  sbox_switchrow_set_state (box, state);
}

gboolean
sbox_switchrow_get_switch (SBoxSwitchRow *box)
{
  g_assert (box);
  return gtk_switch_get_active (box->entry_switch);
}
