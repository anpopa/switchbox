/*
 * Copyright 2021-2022 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sbox-window.h"
#include "sbox-config.h"
#include "sbox-delegate.h"
#include "sbox-switchrow.h"
#include "sbox-types.h"

struct _SBoxWindow {
  AdwApplicationWindow parent_instance;

  /* Application settings */
  GSettings *settings;

  /* List of current entries */
  GList *entry_list;

  /* Helper temp data */
  gsize entry_index;
  gulong entry_id;
  gboolean entry_state;

  /* Main window widgets */
  GtkHeaderBar *main_header_bar;
  GtkListBox *main_list_box;
  GtkSwitch *main_connect_button;
  GMenuModel *main_menu_model;
  GtkMenuButton *main_menu_button;
  GtkPopoverMenu *main_popover_menu;
};

G_DEFINE_TYPE (SBoxWindow, sbox_window, ADW_TYPE_APPLICATION_WINDOW)

static void
sbox_switch_all_on_cb (GtkWidget *widget, const char *action_name, GVariant *parameter)
{
  SBOX_UNUSED (widget);
  SBOX_UNUSED (action_name);
  SBOX_UNUSED (parameter);
  g_debug ("Switch all on");

  {
    SBoxSWEntry *entry = sbox_swentry_new ();
    ExecutorActionPayload pld = { .entry = entry };

    sbox_swentry_set_switch_state (entry, TRUE);
    sbox_executor_push_event (
      sbox_delegate_get_executor (), EXECUTOR_SET_SWENTRY_SWITCH_STATE_ALL, pld);
  }
}

static void
sbox_switch_all_off_cb (GtkWidget *widget, const char *action_name, GVariant *parameter)
{
  SBOX_UNUSED (widget);
  SBOX_UNUSED (action_name);
  SBOX_UNUSED (parameter);
  g_debug ("Switch all off");

  {
    SBoxSWEntry *entry = sbox_swentry_new ();
    ExecutorActionPayload pld = { .entry = entry };

    sbox_swentry_set_switch_state (entry, FALSE);
    sbox_executor_push_event (
      sbox_delegate_get_executor (), EXECUTOR_SET_SWENTRY_SWITCH_STATE_ALL, pld);
  }
}

static void
sbox_show_settings_cb (GtkWidget *widget, const char *action_name, GVariant *parameter)
{
  SBOX_UNUSED (widget);
  SBOX_UNUSED (action_name);
  SBOX_UNUSED (parameter);
  g_debug ("Show settings");
  gtk_window_present (GTK_WINDOW (sbox_delegate_get_settings_window ()));
}

static void
sbox_quit_cb (GtkWidget *widget, const char *action_name, GVariant *parameter)
{
  SBOX_UNUSED (action_name);
  SBOX_UNUSED (parameter);
  g_debug ("Quit");
  gtk_window_destroy (GTK_WINDOW (widget));
}

static void
sbox_notify_system_supports_color_schemes_cb (SBoxWindow *self)
{
  AdwStyleManager *manager = adw_style_manager_get_default ();
  gboolean supports = adw_style_manager_get_system_supports_color_schemes (manager);

  SBOX_UNUSED (self);

  if (supports)
    adw_style_manager_set_color_scheme (manager, ADW_COLOR_SCHEME_DEFAULT);
}

static void
sbox_window_class_init (SBoxWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/ro/fxdata/switchbox/gui/sbox-window.ui");

  /* Grab main menu widgets */
  gtk_widget_class_bind_template_child (widget_class, SBoxWindow, main_header_bar);
  gtk_widget_class_bind_template_child (widget_class, SBoxWindow, main_list_box);
  gtk_widget_class_bind_template_child (widget_class, SBoxWindow, main_connect_button);
  gtk_widget_class_bind_template_child (widget_class, SBoxWindow, main_menu_button);
  gtk_widget_class_bind_template_child (widget_class, SBoxWindow, main_menu_model);

  /* These are the actions that we are using in the menu */
  gtk_widget_class_install_action (
    widget_class, "sbox.switch_all_on", NULL, sbox_switch_all_on_cb);
  gtk_widget_class_install_action (
    widget_class, "sbox.switch_all_off", NULL, sbox_switch_all_off_cb);
  gtk_widget_class_install_action (
    widget_class, "sbox.show_settings", NULL, sbox_show_settings_cb);
  gtk_widget_class_install_action (widget_class, "sbox.quit", NULL, sbox_quit_cb);
}

static gboolean
sbox_connect_switch_activated_cb (GtkSwitch *self, gboolean state, gpointer user_data)
{
  g_autoptr (SBoxExecutor) executor = sbox_delegate_get_executor ();
  ExecutorActionPayload pld = { .entry = NULL };

  SBOX_UNUSED (user_data);
  SBOX_UNUSED (self);

  if (state)
    sbox_executor_push_event (executor, EXECUTOR_CONNECT, pld);
  else
    sbox_executor_push_event (executor, EXECUTOR_DISCONNECT, pld);

  return FALSE;
}

static void
sbox_entry_row_activated_cb (GtkListBox *self, GtkListBoxRow *row, gpointer user_data)
{
  SBoxSwitchRow *box = (SBoxSwitchRow *)row;

  SBOX_UNUSED (self);
  SBOX_UNUSED (user_data);

  g_debug ("Row activated");
  if (sbox_connection_is_connected (sbox_delegate_get_connection ()))
    sbox_switchrow_set_switch (box, !sbox_switchrow_get_switch (box));
}

static void
sbox_menu_init (SBoxWindow *self)
{
  self->main_popover_menu
    = GTK_POPOVER_MENU (gtk_popover_menu_new_from_model (self->main_menu_model));
  gtk_menu_button_set_menu_model (self->main_menu_button, self->main_menu_model);
  gtk_menu_button_set_popover (self->main_menu_button, GTK_WIDGET (self->main_popover_menu));

  g_signal_connect (G_OBJECT (self->main_connect_button),
                    "state-set",
                    G_CALLBACK (sbox_connect_switch_activated_cb),
                    self);

  g_signal_connect (G_OBJECT (self->main_list_box),
                    "row-activated",
                    G_CALLBACK (sbox_entry_row_activated_cb),
                    self);
}

static void
add_entry_list_foreach_cb (gpointer database, gpointer swentry, gpointer arg1, gpointer arg2)
{
  SBoxSWEntry *entry = (SBoxSWEntry *)swentry;
  SBoxWindow *self = (SBoxWindow *)arg1;
  SBoxSwitchRow *new_row = NULL;

  g_assert (self);
  g_assert (entry);
  SBOX_UNUSED (arg2);
  SBOX_UNUSED (database);

  new_row = g_object_new (SBOX_TYPE_SWITCHROW, NULL);

  sbox_switchrow_set_id (new_row, sbox_swentry_get_id (entry));
  sbox_switchrow_set_name (new_row, sbox_swentry_get_name (entry));
  sbox_switchrow_set_desc (new_row, sbox_swentry_get_description (entry));
  sbox_switchrow_set_switch (new_row, sbox_swentry_get_switch_state (entry));

  self->entry_list = g_list_append (self->entry_list, new_row);
  gtk_list_box_append (self->main_list_box, GTK_WIDGET (new_row));
}

static void
sbox_window_add_rows (SBoxWindow *self)
{
  GtkTreeIter iter;

  g_assert (self);
  sbox_database_call_foreach_entry (
    sbox_delegate_get_database (), add_entry_list_foreach_cb, self, &iter, NULL);
}

static void
sbox_load_window_size (SBoxWindow *self)
{
  g_assert (self->settings);

  if (g_settings_get_boolean (self->settings, "is-maximized"))
    {
      gtk_window_maximize (GTK_WINDOW (self));
    }
  else
    {
      gint width, height;

      width = g_settings_get_int (self->settings, "window-width");
      height = g_settings_get_int (self->settings, "window-height");

      gtk_window_set_default_size (GTK_WINDOW (self), width, height);
    }
}

static void
sbox_close_window_signal (SBoxWindow *self, gpointer user_data)
{
  gint width, height;
  gboolean is_maximized;

  g_assert (self->settings);
  SBOX_UNUSED (user_data);

  gtk_window_get_default_size (GTK_WINDOW (self), &width, &height);
  is_maximized = gtk_window_is_maximized (GTK_WINDOW (self));

  g_settings_set_int (self->settings, "window-width", width);
  g_settings_set_int (self->settings, "window-height", height);
  g_settings_set_boolean (self->settings, "is-maximized", is_maximized);
}

static void
sbox_window_init (SBoxWindow *self)
{
  AdwStyleManager *manager = NULL;

  g_assert (self);

  manager = adw_style_manager_get_default ();
  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new ("ro.fxdata.switchbox");
  sbox_load_window_size (self);

  g_signal_connect_object (manager,
                           "notify::system-supports-color-schemes",
                           G_CALLBACK (sbox_notify_system_supports_color_schemes_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect (G_OBJECT (self), "destroy", G_CALLBACK (sbox_close_window_signal), NULL);

  sbox_notify_system_supports_color_schemes_cb (self);
  sbox_menu_init (self);
  sbox_window_add_rows (self);
}

static void
remove_entry_row_cb (gpointer data, gpointer user_data)
{
  SBoxSwitchRow *entry = (SBoxSwitchRow *)data;
  SBoxWindow *win = (SBoxWindow *)user_data;

  g_assert (entry);
  g_assert (win);

  gtk_list_box_remove (win->main_list_box, GTK_WIDGET (entry));
  /* g_object_unref(entry); */
}

void
sbox_window_reload_entries (SBoxWindow *win)
{
  g_assert (win);

  win->entry_index = 0;
  g_list_foreach (win->entry_list, remove_entry_row_cb, win);

  g_list_free (win->entry_list);
  win->entry_list = NULL;

  sbox_window_add_rows (win);
}

static void
update_switch_state_cb (gpointer data, gpointer user_data)
{
  SBoxSwitchRow *entry = (SBoxSwitchRow *)data;
  SBoxWindow *win = (SBoxWindow *)user_data;

  g_assert (entry);
  g_assert (win);

  if (sbox_switchrow_get_id (entry) == win->entry_id)
    sbox_switchrow_set_switch (entry, win->entry_state);
}

void
sbox_window_set_switch_state (SBoxWindow *win, gulong id, gboolean state)
{
  g_assert (win);

  win->entry_id = id;
  win->entry_state = state;
  g_list_foreach (win->entry_list, update_switch_state_cb, win);
}
