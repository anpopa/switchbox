/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define SBOX_TYPE_SWITCHROW (sbox_switchrow_get_type ())

G_DECLARE_FINAL_TYPE (SBoxSwitchRow, sbox_switchrow, SBOX, SWITCHROW, GtkListBoxRow)

void sbox_switchrow_set_id (SBoxSwitchRow *box, gulong id);
gulong sbox_switchrow_get_id (SBoxSwitchRow *box);

void sbox_switchrow_set_name (SBoxSwitchRow *box, const gchar *name);
void sbox_switchrow_set_desc (SBoxSwitchRow *box, const gchar *desc);
void sbox_switchrow_set_state (SBoxSwitchRow *box, gboolean state);
void sbox_switchrow_set_switch (SBoxSwitchRow *box, gboolean state);
gboolean sbox_switchrow_get_switch (SBoxSwitchRow *box);

G_END_DECLS
