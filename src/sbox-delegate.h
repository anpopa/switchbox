/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "core/sbox-connection.h"
#include "core/sbox-database.h"
#include "core/sbox-executor.h"
#include "sbox-types.h"

#include <gtk/gtk.h>
#include <stdlib.h>

G_BEGIN_DECLS

/**
 * @brief Create a new SBoxDelegate object
 * @param error An error object must be provided.
 */
void sbox_delegate_create (const gchar *dbpath, GError **error);

/**
 * @brief Destroy SBoxDelegate object
 */
void sbox_delegate_destroy (void);

/**
 * @brief Get database
 * @return A new referenced database object
 */
SBoxDatabase *sbox_delegate_get_database (void);

/**
 * @brief Get connection
 * @return A new referenced database object
 */
SBoxConnection *sbox_delegate_get_connection (void);

/**
 * @brief Get executor
 * @return A new referenced executor object
 */
SBoxExecutor *sbox_delegate_get_executor (void);

/**
 * @brief Set main window
 */
void sbox_delegate_set_main_window (gpointer win);

/**
 * @brief Get main window
 */
gpointer sbox_delegate_get_main_window (void);

/**
 * @brief Set setting window
 */
void sbox_delegate_set_settings_window (gpointer win);

/**
 * @brief Get setting window
 */
gpointer sbox_delegate_get_settings_window (void);

G_END_DECLS
