/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "sbox-database.h"
#include "sbox-types.h"

#include <glib.h>
#include <mosquitto.h>
#include <pthread.h>
#include <stdbool.h>

G_BEGIN_DECLS

/**
 * @enum connection_event_data
 * @brief Connection event data type
 */
typedef enum _ConnectionEventType {
  CONN_EVENT_MQTT_START,
  CONN_EVENT_MQTT_STOP
} ConnectionEventType;

typedef gboolean (*SBoxConnectionCallback)(gpointer _connection, gpointer _event);

/**
 * @struct SBoxConnectionEvent
 */
typedef struct _SBoxConnectionEvent {
  ConnectionEventType type;   /**< The event type the element holds */
} SBoxConnectionEvent;

typedef struct _SBoxConnection {
  GSource source;                    /**< Event loop source */
  GAsyncQueue *queue;                /**< Async queue */
  SBoxConnectionCallback callback;   /**< Callback function */
  SBoxDatabase *database;

  pthread_t thread;
  gboolean thread_stop;
  struct mosquitto *mosq;
  gchar *client_id;

  grefcount rc;   /**< Reference counter variable  */
} SBoxConnection;

/*
 * @brief Create a new connection object
 * @return On success return a new SBoxConnection object
 */
SBoxConnection *sbox_connection_new (SBoxDatabase *database);

/**
 * @brief Aquire connection object
 * @return The referenced connection object
 */
SBoxConnection *sbox_connection_ref (SBoxConnection *connection);

/**
 * @brief Release connection object
 */
void sbox_connection_unref (SBoxConnection *connection);

/**
 * @brief Connect
 */
void sbox_connection_connect (SBoxConnection *connection);

/**
 * @brief Disconnect
 */
void sbox_connection_disconnect (SBoxConnection *connection);

/**s
 * @brief Check if connection thread is running
 */
gboolean sbox_connection_is_connected (SBoxConnection *connection);

/**
 * @brief Set Switch state
 */
void sbox_connection_set_switch_state (SBoxConnection *connection,
                                       const gchar *topic,
                                       const gchar *state);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (SBoxConnection, sbox_connection_unref);

G_END_DECLS
