/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sbox-database.h"
#include "sbox-swentry.h"

/**
 * @enum Database query type
 */
typedef enum _DatabaseQueryType {
  QUERY_CREATE_TABLES,
  QUERY_ADD_CONN_CONF,
  QUERY_HAS_CONN_CONF,
  QUERY_ADD_SWENTRY,
  QUERY_HAS_SWENTRY,
  QUERY_REM_SWENTRY,
  QUERY_GET_SWENTRY,
  QUERY_FOREACH_ENTRY,
  QUERY_SET_HADDR,
  QUERY_SET_HPORT,
  QUERY_GET_HADDR,
  QUERY_GET_HPORT
} DatabaseQueryType;

/**
 * @enum Database query data object
 */
typedef struct _DatabaseQueryData {
  DatabaseQueryType type;
  gpointer response;
  gpointer arg1;
  gpointer arg2;
  SBoxDatabase *database;
  SBoxDatabaseCallback callback;
} DatabaseQueryData;

/**
 * @struct Add swentry object helper
 */
typedef struct _DatabaseAddSWEntry {
  SBoxDatabase *database;
  SBoxSWEntry *swentry;
} DatabaseAddSWEntry;

const gchar *sbox_table_switches = "Switches";
const gchar *sbox_table_connection = "Connection";

/**
 * @brief SQlite3 callback
 */
static int sqlite_callback (void *data, int argc, char **argv, char **colname);
/**
 * @brief Add default connection config
 */
static SBoxStatus add_default_connection_config (SBoxDatabase *database, GError **error);

static int
sqlite_callback (void *data, int argc, char **argv, char **colname)
{
  DatabaseQueryData *querydata = (DatabaseQueryData *)data;

  switch (querydata->type)
    {
    case QUERY_CREATE_TABLES:
      break;

    case QUERY_ADD_SWENTRY:
      break;

    case QUERY_ADD_CONN_CONF:
      break;

    case QUERY_REM_SWENTRY:
      break;

    case QUERY_SET_HADDR:
      break;

    case QUERY_SET_HPORT:
      break;

    case QUERY_HAS_CONN_CONF:
      for (gint i = 0; i < argc; i++)
        {
          if (g_strcmp0 (colname[i], "NAME") == 0)
            *((gboolean *)querydata->response) = TRUE;
        }
      break;

    case QUERY_HAS_SWENTRY:
      for (gint i = 0; i < argc; i++)
        {
          if (g_strcmp0 (colname[i], "NAME") == 0)
            *((gboolean *)querydata->response) = TRUE;
        }
      break;

    case QUERY_GET_HADDR:
      for (gint i = 0; i < argc; i++)
        {
          if (g_strcmp0 (colname[i], "HADDR") == 0)
            querydata->response = (gpointer)g_strdup (argv[i]);
        }
      break;

    case QUERY_GET_HPORT:
      for (gint i = 0; i < argc; i++)
        {
          if (g_strcmp0 (colname[i], "HPORT") == 0)
            *((glong *)(querydata->response)) = (glong)g_ascii_strtoll (argv[i], NULL, 10);
        }
      break;

    case QUERY_GET_SWENTRY: {
      SBoxSWEntry *entry = sbox_swentry_new ();

      for (gint i = 0; i < argc; i++)
        {
          if (g_strcmp0 (colname[i], "ID") == 0)
            sbox_swentry_set_id (entry, g_ascii_strtoull (argv[i], NULL, 10));
          else if (g_strcmp0 (colname[i], "NAME") == 0)
            sbox_swentry_set_name (entry, argv[i]);
          else if (g_strcmp0 (colname[i], "DESC") == 0)
            sbox_swentry_set_description (entry, argv[i]);
          else if (g_strcmp0 (colname[i], "TOPIC") == 0)
            sbox_swentry_set_topic (entry, argv[i]);
          else if (g_strcmp0 (colname[i], "PLDON") == 0)
            sbox_swentry_set_payload_on (entry, argv[i]);
          else if (g_strcmp0 (colname[i], "PLDOFF") == 0)
            sbox_swentry_set_payload_off (entry, argv[i]);
        }

      querydata->response = entry;
    } break;

    case QUERY_FOREACH_ENTRY: {
      g_autoptr (SBoxSWEntry) entry = sbox_swentry_new ();

      for (gint i = 0; i < argc; i++)
        {
          if (g_strcmp0 (colname[i], "ID") == 0)
            sbox_swentry_set_id (entry, g_ascii_strtoull (argv[i], NULL, 10));
          else if (g_strcmp0 (colname[i], "NAME") == 0)
            sbox_swentry_set_name (entry, argv[i]);
          else if (g_strcmp0 (colname[i], "DESC") == 0)
            sbox_swentry_set_description (entry, argv[i]);
          else if (g_strcmp0 (colname[i], "TOPIC") == 0)
            sbox_swentry_set_topic (entry, argv[i]);
          else if (g_strcmp0 (colname[i], "PLDON") == 0)
            sbox_swentry_set_payload_on (entry, argv[i]);
          else if (g_strcmp0 (colname[i], "PLDOFF") == 0)
            sbox_swentry_set_payload_off (entry, argv[i]);
        }

      querydata->callback (querydata->database, entry, querydata->arg1, querydata->arg2);
    } break;

    default:
      break;
    }

  return 0;
}

SBoxDatabase *
sbox_database_new (const gchar *dbfile, GError **error)
{
  SBoxDatabase *database = NULL;
  gchar *query_error = NULL;

  DatabaseQueryData data
    = { .type = QUERY_CREATE_TABLES, .database = database, .callback = NULL, .response = NULL };

  database = g_new0 (SBoxDatabase, 1);

  g_assert (database);

  g_ref_count_init (&database->rc);

  if (sqlite3_open (dbfile, &database->database))
    {
      g_warning ("Cannot open database database at path %s", dbfile);
      g_set_error (error, g_quark_from_static_string ("DatabaseNew"), 1, "Database open failed");
    }
  else
    {
      g_autofree gchar *switches_sql = NULL;

      switches_sql = g_strdup_printf ("CREATE TABLE IF NOT EXISTS %s        "
                                      "(ID      UNSIGNED INTEGER PRIMARY KEY NOT NULL, "
                                      " NAME      TEXT            NOT NULL, "
                                      " DESC      TEXT            NOT NULL, "
                                      " TOPIC     TEXT            NOT NULL, "
                                      " PLDON     TEXT            NOT NULL, "
                                      " PLDOFF    TEXT            NOT NULL);",
                                      sbox_table_switches);

      if (sqlite3_exec (database->database, switches_sql, sqlite_callback, &data, &query_error)
          != SQLITE_OK)
        {
          g_warning ("Fail to create switches table. SQL error %s", query_error);
          g_set_error (
            error, g_quark_from_static_string ("DatabaseNew"), 1, "Create switches table fail");
        }
      else
        {
          g_autofree gchar *connection_sql = NULL;

          connection_sql = g_strdup_printf ("CREATE TABLE IF NOT EXISTS %s        "
                                            "(NAME     TEXT       NOT   NULL, "
                                            " HADDR    TEXT       NOT   NULL, "
                                            " HPORT    NUMERIC    NOT   NULL);",
                                            sbox_table_connection);

          if (sqlite3_exec (
                database->database, connection_sql, sqlite_callback, &data, &query_error)
              != SQLITE_OK)
            {
              g_warning ("Fail to create connection table. SQL error %s", query_error);
              g_set_error (error,
                           g_quark_from_static_string ("DatabaseNew"),
                           1,
                           "Create connection table failed");
            }
          else
            {
              if (add_default_connection_config (database, error) != SBOX_STATUS_OK)
                g_error ("Fail to insert default connection data");
            }
        }
    }

  return database;
}

SBoxDatabase *
sbox_database_ref (SBoxDatabase *database)
{
  g_assert (database);
  g_ref_count_inc (&database->rc);
  return database;
}

void
sbox_database_unref (SBoxDatabase *database)
{
  g_assert (database);

  if (g_ref_count_dec (&database->rc) == TRUE)
    {
      g_free (database);
    }
}

static gboolean
has_mqtt_connection (SBoxDatabase *database, GError **error)
{
  g_autofree gchar *sql = NULL;
  gchar *query_error = NULL;
  gboolean result = FALSE;

  DatabaseQueryData data = {
    .type = QUERY_HAS_CONN_CONF, .database = database, .callback = NULL, .response = &result
  };

  g_assert (database);

  sql = g_strdup_printf ("SELECT NAME FROM %s WHERE NAME IS '%s'", sbox_table_connection, "mqtt");

  if (sqlite3_exec (database->database, sql, sqlite_callback, &data, &query_error) != SQLITE_OK)
    {
      g_set_error (
        error, g_quark_from_static_string ("DatabaseHasConnectionEntry"), 1, "SQL query error");
      g_warning ("Fail to execute has connection entry. SQL error %s", query_error);
      sqlite3_free (query_error);
    }

  return result;
}

static SBoxStatus
add_default_connection_config (SBoxDatabase *database, GError **error)
{
  g_autofree gchar *sql = NULL;
  gchar *query_error = NULL;

  DatabaseQueryData data
    = { .type = QUERY_ADD_CONN_CONF, .database = database, .callback = NULL, .response = NULL };

  g_assert (database);

  if (has_mqtt_connection (database, error) == TRUE)
    return SBOX_STATUS_OK;

  sql = g_strdup_printf ("INSERT INTO %s                     "
                         "(NAME, HADDR, HPORT)     "
                         "VALUES('%s', '%s', %d);  ",
                         sbox_table_connection,
                         "mqtt",
                         "localhost",
                         1883);

  if (sqlite3_exec (database->database, sql, sqlite_callback, &data, &query_error) != SQLITE_OK)
    {
      g_set_error (error,
                   g_quark_from_static_string ("DatabaseAddConnectionDefaults"),
                   1,
                   "SQL query error");
      g_warning ("Fail to add default connection entry. SQL error %s", query_error);
      sqlite3_free (query_error);

      return SBOX_STATUS_ERROR;
    }

  return SBOX_STATUS_OK;
}

SBoxStatus
sbox_database_add_swentry (SBoxDatabase *database, SBoxSWEntry *swentry, GError **error)
{
  g_autofree gchar *sql = NULL;
  gchar *query_error = NULL;

  DatabaseQueryData data
    = { .type = QUERY_ADD_SWENTRY, .database = database, .callback = NULL, .response = NULL };

  g_assert (database);
  g_assert (swentry);

  sql = g_strdup_printf ("INSERT INTO %s                     "
                         "(ID,NAME,DESC,TOPIC,PLDON,PLDOFF)     "
                         "VALUES(%lu, '%s', '%s', '%s', '%s', '%s');  ",
                         sbox_table_switches,
                         sbox_swentry_get_id (swentry),
                         sbox_swentry_get_name (swentry),
                         sbox_swentry_get_description (swentry),
                         sbox_swentry_get_topic (swentry),
                         sbox_swentry_get_payload_on (swentry),
                         sbox_swentry_get_payload_off (swentry));

  if (sqlite3_exec (database->database, sql, sqlite_callback, &data, &query_error) != SQLITE_OK)
    {
      g_set_error (error, g_quark_from_static_string ("DatabaseAddSwitch"), 1, "SQL query error");
      g_warning ("Fail to add new switch entry. SQL error %s", query_error);
      sqlite3_free (query_error);

      return SBOX_STATUS_ERROR;
    }

  return SBOX_STATUS_OK;
}

SBoxSWEntry *
sbox_database_get_swentry (SBoxDatabase *database, gulong id, GError **error)
{
  g_autofree gchar *sql = NULL;
  gchar *query_error = NULL;

  DatabaseQueryData data
    = { .type = QUERY_GET_SWENTRY, .database = database, .callback = NULL, .response = NULL };

  g_assert (database);

  sql = g_strdup_printf ("SELECT * FROM %s WHERE ID IS %lu", sbox_table_switches, id);

  if (sqlite3_exec (database->database, sql, sqlite_callback, &data, &query_error) != SQLITE_OK)
    {
      g_set_error (
        error, g_quark_from_static_string ("DatabaseGetSwitchEntry"), 1, "SQL query error");
      g_warning ("Fail to get switch entry. SQL error %s", query_error);
      sqlite3_free (query_error);
    }

  return (SBoxSWEntry *)data.response;
}

gboolean
sbox_database_has_swentry (SBoxDatabase *database, gulong id, GError **error)
{
  g_autofree gchar *sql = NULL;
  gchar *query_error = NULL;
  gboolean result = FALSE;

  DatabaseQueryData data
    = { .type = QUERY_HAS_SWENTRY, .database = database, .callback = NULL, .response = &result };

  g_assert (database);

  sql = g_strdup_printf ("SELECT NAME FROM %s WHERE ID IS %lu", sbox_table_switches, id);

  if (sqlite3_exec (database->database, sql, sqlite_callback, &data, &query_error) != SQLITE_OK)
    {
      g_set_error (
        error, g_quark_from_static_string ("DatabaseHasSwitchEntry"), 1, "SQL query error");
      g_warning ("Fail to execute has switch entry. SQL error %s", query_error);
      sqlite3_free (query_error);
    }

  return result;
}

SBoxStatus
sbox_database_rem_swentry (SBoxDatabase *database, gulong id, GError **error)
{
  g_autofree gchar *sql = NULL;
  gchar *query_error = NULL;

  DatabaseQueryData data
    = { .type = QUERY_REM_SWENTRY, .database = database, .callback = NULL, .response = NULL };

  g_assert (database);

  sql = g_strdup_printf ("DELETE FROM %s WHERE ID IS %lu", sbox_table_switches, id);

  if (sqlite3_exec (database->database, sql, sqlite_callback, &data, &query_error) != SQLITE_OK)
    {
      g_set_error (
        error, g_quark_from_static_string ("DatabaseRemoveSWEntry"), 1, "SQL query error");
      g_warning ("Fail to remove switch entry. SQL error %s", query_error);
      sqlite3_free (query_error);
      return SBOX_STATUS_ERROR;
    }

  return SBOX_STATUS_OK;
}

void
sbox_database_call_foreach_entry (SBoxDatabase *database,
                                  SBoxDatabaseCallback callback,
                                  gpointer arg1,
                                  gpointer arg2,
                                  GError **error)
{
  g_autofree gchar *sql = NULL;
  gchar *query_error = NULL;

  DatabaseQueryData data = { .type = QUERY_FOREACH_ENTRY,
                             .database = database,
                             .callback = callback,
                             .arg1 = arg1,
                             .arg2 = arg2,
                             .response = NULL };

  g_assert (database);

  sql = g_strdup_printf ("SELECT * FROM %s", sbox_table_switches);

  if (sqlite3_exec (database->database, sql, sqlite_callback, &data, &query_error) != SQLITE_OK)
    {
      g_set_error (
        error, g_quark_from_static_string ("DatabaseForeachEntry"), 1, "SQL query error");
      g_warning ("Fail to execute foreach entry. SQL error %s", query_error);
      sqlite3_free (query_error);
    }
}

SBoxStatus
sbox_database_set_connection_host_addr (SBoxDatabase *database, const gchar *haddr, GError **error)
{
  g_autofree gchar *sql = NULL;
  gchar *query_error = NULL;
  SBoxStatus status = SBOX_STATUS_OK;

  DatabaseQueryData data
    = { .type = QUERY_SET_HADDR, .database = database, .callback = NULL, .response = NULL };

  g_assert (database);
  g_assert (haddr);

  sql = g_strdup_printf (
    "UPDATE %s SET HADDR = '%s' WHERE NAME IS '%s'", sbox_table_connection, haddr, "mqtt");

  if (sqlite3_exec (database->database, sql, sqlite_callback, &data, &query_error) != SQLITE_OK)
    {
      g_set_error (
        error, g_quark_from_static_string ("DatabaseSetConnectionHAddr"), 1, "SQL query error");
      g_warning ("Fail to set connection haddr. SQL error %s", query_error);
      sqlite3_free (query_error);
      status = SBOX_STATUS_ERROR;
    }

  return status;
}

SBoxStatus
sbox_database_set_connection_host_port (SBoxDatabase *database, glong port, GError **error)
{
  g_autofree gchar *sql = NULL;
  gchar *query_error = NULL;
  SBoxStatus status = SBOX_STATUS_OK;

  DatabaseQueryData data
    = { .type = QUERY_SET_HPORT, .database = database, .callback = NULL, .response = NULL };

  g_assert (database);

  sql = g_strdup_printf (
    "UPDATE %s SET HPORT = %ld WHERE NAME IS '%s'", sbox_table_connection, port, "mqtt");

  if (sqlite3_exec (database->database, sql, sqlite_callback, &data, &query_error) != SQLITE_OK)
    {
      g_set_error (
        error, g_quark_from_static_string ("DatabaseSetConnectionHPort"), 1, "SQL query error");
      g_warning ("Fail to set connection hport. SQL error %s", query_error);
      sqlite3_free (query_error);
      status = SBOX_STATUS_ERROR;
    }

  return status;
}

gchar *
sbox_database_get_connection_host_addr (SBoxDatabase *database, GError **error)
{
  g_autofree gchar *sql = NULL;
  gchar *query_error = NULL;

  DatabaseQueryData data
    = { .type = QUERY_GET_HADDR, .database = database, .callback = NULL, .response = NULL };

  g_assert (database);

  sql = g_strdup_printf ("SELECT HADDR FROM %s WHERE NAME IS '%s'", sbox_table_connection, "mqtt");

  if (sqlite3_exec (database->database, sql, sqlite_callback, &data, &query_error) != SQLITE_OK)
    {
      g_set_error (error, g_quark_from_static_string ("DatabaseGetConHaddr"), 1, "SQL query error");
      g_warning ("Fail to execute get connection haddr. SQL error %s", query_error);
      sqlite3_free (query_error);
    }

  return (gchar *)data.response;
}

glong
sbox_database_get_connection_host_port (SBoxDatabase *database, GError **error)
{
  g_autofree gchar *sql = NULL;
  gchar *query_error = NULL;
  glong result = -1;

  DatabaseQueryData data
    = { .type = QUERY_GET_HPORT, .database = database, .callback = NULL, .response = &result };

  g_assert (database);

  sql = g_strdup_printf ("SELECT HPORT FROM %s WHERE NAME IS '%s'", sbox_table_connection, "mqtt");

  if (sqlite3_exec (database->database, sql, sqlite_callback, &data, &query_error) != SQLITE_OK)
    {
      g_set_error (error, g_quark_from_static_string ("DatabaseGetConHPort"), 1, "SQL query error");
      g_warning ("Fail to execute get connection hport. SQL error %s", query_error);
      sqlite3_free (query_error);
    }

  return result;
}
