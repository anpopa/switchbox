/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "sbox-swentry.h"
#include "sbox-types.h"

#include <glib.h>
#include <sqlite3.h>

G_BEGIN_DECLS

typedef void (*SBoxDatabaseCallback)(gpointer _database,
                                     gpointer _swentry,
                                     gpointer arg1,
                                     gpointer arg2);

/**
 * @struct SBoxDatabase
 * @brief The SBoxDatabase opaque data structure
 */
typedef struct _SBoxDatabase {
  sqlite3 *database;   /**< The sqlite3 database object */
  grefcount rc;        /**< Reference counter variable  */
} SBoxDatabase;

/**
 * @brief Create a new database object
 * @param path Path to database file
 * @param error The GError object or NULL
 * @return On success return a new SBoxDatabase object otherwise return NULL
 */
SBoxDatabase *sbox_database_new (const gchar *dbfile, GError **error);

/**
 * @brief Aquire database object
 * @param database Pointer to the database object
 * @return The referenced database object
 */
SBoxDatabase *sbox_database_ref (SBoxDatabase *database);

/**
 * @brief Release an database object
 * @param database Pointer to the database object
 */
void sbox_database_unref (SBoxDatabase *database);

/**
 * @brief Add new swentry in database
 * @param database Pointer to the database object
 * @param swentry The new swentry object
 * @param error The GError object or NULL
 * @return On success return SBOX_STATUS_OK
 */
SBoxStatus sbox_database_add_swentry (SBoxDatabase *database, SBoxSWEntry *swentry, GError **error);

/**
 * @brief Has swentry in database
 * @param database Pointer to the database object
 * @param id The swentry id
 * @param error The GError object or NULL
 * @return On success return SBOX_STATUS_OK
 */
gboolean sbox_database_has_swentry (SBoxDatabase *database, gulong id, GError **error);

/**
 * @brief Foreach entry call in database parser
 * @param database Pointer to the database object
 * @param callback callback function
 * @param error The GError object or NULL
 */
void sbox_database_call_foreach_entry (SBoxDatabase *database,
                                       SBoxDatabaseCallback callback,
                                       gpointer arg1,
                                       gpointer arg2,
                                       GError **error);
/**
 * @brief Get swentry in database
 * @param database Pointer to the database object
 * @param id The swentry id
 * @param error The GError object or NULL
 * @return On success return SBOX_STATUS_OK
 */
SBoxSWEntry *sbox_database_get_swentry (SBoxDatabase *database, gulong id, GError **error);

/**
 * @brief Remove swentry in database
 * @param database Pointer to the database object
 * @param id The swentry id
 * @param error The GError object or NULL
 * @return On success return SBOX_STATUS_OK
 */
gboolean sbox_database_rem_swentry (SBoxDatabase *database, gulong id, GError **error);

/**
 * @brief Set connection host addr
 * @param database Pointer to the database object
 * @param haddr Host address string
 * @param error The GError object or NULL
 * @return On success return SBOX_STATUS_OK
 */
SBoxStatus
sbox_database_set_connection_host_addr (SBoxDatabase *database, const gchar *haddr, GError **error);

/**
 * @brief Get connection host addr
 * @param database Pointer to the database object
 * @param error The GError object or NULL
 * @return On success return new allocated host address string
 */
gchar *sbox_database_get_connection_host_addr (SBoxDatabase *database, GError **error);

/**
 * @brief Set connection host port
 * @param database Pointer to the database object
 * @param hport Host port
 * @param error The GError object or NULL
 * @return On success return SBOX_STATUS_OK
 */
SBoxStatus
sbox_database_set_connection_host_port (SBoxDatabase *database, glong hport, GError **error);

/**
 * @brief Get connection host port
 * @param database Pointer to the database object
 * @param error The GError object or NULL
 * @return On success return SBOX_STATUS_OK
 */
glong sbox_database_get_connection_host_port (SBoxDatabase *database, GError **error);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (SBoxDatabase, sbox_database_unref);

G_END_DECLS
