/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sbox-executor.h"
#include "gui/sbox-window.h"
#include "sbox-connection.h"
#include "sbox-database.h"
#include "sbox-delegate.h"

#include <gtk/gtk.h>

/**
 * @brief Post new event
 * @param executor A pointer to the executor object
 * @param type The type of the new event to be posted
 * @param payload Action payload
 */
static void
post_executor_event (SBoxExecutor *executor, ExecutorEventType type, ExecutorActionPayload payload);
/**
 * @brief GSource prepare function
 */
static gboolean executor_source_prepare (GSource *source, gint *timeout);
/**
 * @brief GSource dispatch function
 */
static gboolean executor_source_dispatch (GSource *source, GSourceFunc callback,
                                          gpointer _executor);
/**
 * @brief GSource callback function
 */
static gboolean executor_source_callback (gpointer _executor, gpointer _event);
/**
 * @brief GSource destroy notification callback function
 */
static void executor_source_destroy_notify (gpointer _executor);
/**
 * @brief Async queue destroy notification callback function
 */
static void executor_queue_destroy_notify (gpointer _executor);
/**
 * @brief Process connect event
 */
static void do_connect_event (SBoxExecutor *executor);
/**
 * @brief Process disconnect event
 */
static void do_disconnect_event (SBoxExecutor *executor);
/**
 * @brief Process set switch state
 */
static void do_set_switch_event (SBoxExecutor *executor, SBoxExecutorEvent *event);
/**
 * @brief Process set switch state from cloud
 */
static void do_set_switch_event_cloud (SBoxExecutor *executor, SBoxExecutorEvent *event);
/**
 * @brief Process add swentry event
 */
static void do_add_entry_event (SBoxExecutor *executor, SBoxExecutorEvent *event);
/**
 * @brief Process rem swentry event
 */
static void do_rem_entry_event (SBoxExecutor *executor, SBoxExecutorEvent *event);
/**
 * @brief Process set switch state all
 */
static void do_set_switch_all_event (SBoxExecutor *executor, SBoxExecutorEvent *event);

/**
 * @brief GSourceFuncs vtable
 */
static GSourceFuncs executor_source_funcs = {
  executor_source_prepare,
  NULL,
  executor_source_dispatch,
  NULL,
  NULL,
  NULL,
};

static void
post_executor_event (SBoxExecutor *executor, ExecutorEventType type, ExecutorActionPayload payload)
{
  SBoxExecutorEvent *e = NULL;

  g_assert (executor);

  e = g_new0 (SBoxExecutorEvent, 1);
  e->type = type;
  e->payload = payload;
  g_async_queue_push (executor->queue, e);
}

static gboolean
executor_source_prepare (GSource *source, gint *timeout)
{
  SBoxExecutor *executor = (SBoxExecutor *)source;

  SBOX_UNUSED (timeout);

  return(g_async_queue_length (executor->queue) > 0);
}

static gboolean
executor_source_dispatch (GSource *source, GSourceFunc callback, gpointer _executor)
{
  SBoxExecutor *executor = (SBoxExecutor *)source;
  gpointer event = g_async_queue_try_pop (executor->queue);

  SBOX_UNUSED (callback);
  SBOX_UNUSED (_executor);

  if (event == NULL)
    return G_SOURCE_CONTINUE;

  return executor->callback (executor, event) == TRUE ? G_SOURCE_CONTINUE : G_SOURCE_REMOVE;
}

static gboolean
executor_source_callback (gpointer _executor, gpointer _event)
{
  SBoxExecutor *executor = (SBoxExecutor *)_executor;
  SBoxExecutorEvent *event = (SBoxExecutorEvent *)_event;

  g_assert (executor);
  g_assert (event);

  switch (event->type)
    {
    case EXECUTOR_CONNECT:
      do_connect_event (executor);
      break;

    case EXECUTOR_DISCONNECT:
      do_disconnect_event (executor);
      break;

    case EXECUTOR_SET_SWENTRY_SWITCH_STATE:
      do_set_switch_event (executor, event);
      break;

    case EXECUTOR_SET_SWENTRY_SWITCH_STATE_ALL:
      do_set_switch_all_event (executor, event);
      break;

    case EXECUTOR_SET_SWENTRY_SWITCH_STATE_CLOUD:
      do_set_switch_event_cloud (executor, event);
      break;

    case EXECUTOR_ADD_SWENTRY:
      do_add_entry_event (executor, event);
      break;

    case EXECUTOR_REM_SWENTRY:
      do_rem_entry_event (executor, event);
      break;

    default:
      g_warning ("Executor unknown event='%d'", event->type);
      break;
    }

  if (event->payload.entry != NULL)
    sbox_swentry_unref (event->payload.entry);
  g_free (event);

  return TRUE;
}

static void
do_connect_event (SBoxExecutor *executor)
{
  g_assert (executor);

  g_debug ("Process connect event");
  sbox_connection_connect (executor->connection);
}

static void
do_disconnect_event (SBoxExecutor *executor)
{
  g_assert (executor);

  g_debug ("Process disconnect event");
  sbox_connection_disconnect (executor->connection);
}

static void
do_set_switch_event (SBoxExecutor *executor, SBoxExecutorEvent *event)
{
  SBoxSWEntry *entry = NULL;

  g_assert (executor);
  g_assert (event);

  g_debug ("Process set switch state event");
  entry = event->payload.entry;

  if (!sbox_database_has_swentry (executor->database, sbox_swentry_get_id (entry), NULL))
    gtk_switch_set_active (GTK_SWITCH (sbox_swentry_get_widget (entry)), FALSE);
  else
    {
      g_autoptr (SBoxSWEntry) dbentry
        = sbox_database_get_swentry (executor->database, sbox_swentry_get_id (entry), NULL);
      const gchar *state = sbox_swentry_get_switch_state (entry)
                                 ? sbox_swentry_get_payload_on (dbentry)
                                 : sbox_swentry_get_payload_off (dbentry);
      sbox_connection_set_switch_state (
        executor->connection, sbox_swentry_get_topic (dbentry), state);
    }

  /* Release widget */
  g_object_unref (sbox_swentry_get_widget (entry));
}

static void
entry_set_foreach_cb (gpointer database, gpointer swentry, gpointer arg1, gpointer arg2)
{
  SBoxExecutor *executor = (SBoxExecutor *)arg1;
  SBoxSWEntry *request_entry = (SBoxSWEntry *)arg2;
  SBoxSWEntry *entry = (SBoxSWEntry *)swentry;
  const gchar *state = sbox_swentry_get_switch_state (request_entry)
                             ? sbox_swentry_get_payload_on (entry)
                             : sbox_swentry_get_payload_off (entry);

  SBOX_UNUSED (database);

  sbox_connection_set_switch_state (executor->connection, sbox_swentry_get_topic (entry), state);
}

static void
do_set_switch_all_event (SBoxExecutor *executor, SBoxExecutorEvent *event)
{
  SBoxSWEntry *entry = NULL;

  g_assert (executor);
  g_assert (event);

  g_debug ("Process set switch state event");
  entry = event->payload.entry;

  sbox_database_call_foreach_entry (
    executor->database, entry_set_foreach_cb, executor, entry, NULL);
}

static void
do_set_switch_event_cloud (SBoxExecutor *executor, SBoxExecutorEvent *event)
{
  SBoxSWEntry *entry = NULL;

  g_assert (executor);
  g_assert (event);

  g_debug ("Process set switch state event cloud");
  entry = event->payload.entry;

  sbox_window_set_switch_state (SBOX_WINDOW (sbox_delegate_get_main_window ()),
                                sbox_swentry_get_id (entry),
                                sbox_swentry_get_switch_state (entry));
}

static void
do_add_entry_event (SBoxExecutor *executor, SBoxExecutorEvent *event)
{
  SBoxSWEntry *entry = NULL;
  gboolean exist = FALSE;

  g_assert (executor);
  g_assert (event);

  g_debug ("Process add swentry");
  entry = event->payload.entry;
  exist = sbox_database_has_swentry (executor->database, sbox_swentry_get_id (entry), NULL);

  if (exist)
    sbox_database_rem_swentry (executor->database, sbox_swentry_get_id (entry), NULL);

  sbox_database_add_swentry (executor->database, entry, NULL);
  sbox_window_reload_entries (SBOX_WINDOW (sbox_delegate_get_main_window ()));
}

static void
do_rem_entry_event (SBoxExecutor *executor, SBoxExecutorEvent *event)
{
  SBoxSWEntry *entry = NULL;
  gboolean exist = FALSE;

  g_assert (executor);
  g_assert (event);

  g_debug ("Process remove swentry");
  entry = event->payload.entry;
  exist = sbox_database_has_swentry (executor->database, sbox_swentry_get_id (entry), NULL);

  if (exist)
    {
      sbox_database_rem_swentry (executor->database, sbox_swentry_get_id (entry), NULL);
      sbox_window_reload_entries (SBOX_WINDOW (sbox_delegate_get_main_window ()));
    }
  else
    g_warning ("Try to remove unexisting swentry ID=%ld", sbox_swentry_get_id (entry));
}

static void
executor_source_destroy_notify (gpointer _executor)
{
  SBoxExecutor *executor = (SBoxExecutor *)_executor;

  g_assert (executor);
  g_debug ("Executor destroy notification");

  sbox_executor_unref (executor);
}

static void
executor_queue_destroy_notify (gpointer _executor)
{
  SBOX_UNUSED (_executor);
  g_debug ("Executor queue destroy notification");
}

SBoxExecutor *
sbox_executor_new (SBoxConnection *connection, SBoxDatabase *database)
{
  SBoxExecutor *executor
    = (SBoxExecutor *)g_source_new (&executor_source_funcs, sizeof(SBoxExecutor));

  g_assert (executor);

  g_ref_count_init (&executor->rc);
  executor->connection = sbox_connection_ref (connection);
  executor->database = sbox_database_ref (database);
  executor->callback = executor_source_callback;
  executor->queue = g_async_queue_new_full (executor_queue_destroy_notify);

  g_source_set_callback (
    SBOX_EVENT_SOURCE (executor), NULL, executor, executor_source_destroy_notify);
  g_source_attach (SBOX_EVENT_SOURCE (executor), NULL);

  return executor;
}

SBoxExecutor *
sbox_executor_ref (SBoxExecutor *executor)
{
  g_assert (executor);
  g_ref_count_inc (&executor->rc);

  return executor;
}

void
sbox_executor_unref (SBoxExecutor *executor)
{
  g_assert (executor);

  if (g_ref_count_dec (&executor->rc) == TRUE)
    {
      if (executor->connection != NULL)
        sbox_connection_unref ((SBoxConnection *)executor->connection);

      if (executor->database != NULL)
        sbox_database_unref ((SBoxDatabase *)executor->database);

      g_async_queue_unref (executor->queue);
      g_source_unref (SBOX_EVENT_SOURCE (executor));
    }
}

void
sbox_executor_push_event (SBoxExecutor *executor,
                          ExecutorEventType type,
                          ExecutorActionPayload payload)
{
  post_executor_event (executor, type, payload);
  g_main_context_wakeup (NULL);
}
