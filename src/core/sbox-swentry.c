/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sbox-swentry.h"

static guint64
jenkins_hash (const gchar *key)
{
  guint64 hash, i;

  for (hash = i = 0; i < strlen (key); ++i)
    {
      hash += (guint64)key[i];
      hash += (hash << 10);
      hash ^= (hash >> 6);
    }

  hash += (hash << 3);
  hash ^= (hash >> 11);
  hash += (hash << 15);

  return hash;
}

SBoxSWEntry *
sbox_swentry_new (void)
{
  SBoxSWEntry *swentry = g_new0 (SBoxSWEntry, 1);

  g_ref_count_init (&swentry->rc);

  return swentry;
}

SBoxSWEntry *
sbox_swentry_ref (SBoxSWEntry *swentry)
{
  g_assert (swentry);
  g_ref_count_inc (&swentry->rc);
  return swentry;
}

void
sbox_swentry_unref (SBoxSWEntry *swentry)
{
  g_assert (swentry);

  if (g_ref_count_dec (&swentry->rc) == TRUE)
    {
      if (swentry->name != NULL)
        g_free (swentry->name);

      if (swentry->description != NULL)
        g_free (swentry->description);

      if (swentry->topic != NULL)
        g_free (swentry->topic);

      if (swentry->payload_on != NULL)
        g_free (swentry->payload_on);

      if (swentry->payload_off != NULL)
        g_free (swentry->payload_off);

      g_free (swentry);
    }
}

void
sbox_swentry_set_id (SBoxSWEntry *swentry, gulong id)
{
  g_assert (swentry);
  swentry->id = id;
}

gulong
sbox_swentry_get_id (SBoxSWEntry *swentry)
{
  g_assert (swentry);
  return swentry->id;
}

void
sbox_swentry_set_switch_state (SBoxSWEntry *swentry, gboolean state)
{
  g_assert (swentry);
  swentry->state = state;
}

gboolean
sbox_swentry_get_switch_state (SBoxSWEntry *swentry)
{
  g_assert (swentry);
  return swentry->state;
}

void
sbox_swentry_set_widget (SBoxSWEntry *swentry, gpointer widget)
{
  g_assert (swentry);
  swentry->widget = widget;
}

gpointer
sbox_swentry_get_widget (SBoxSWEntry *swentry)
{
  g_assert (swentry);
  return swentry->widget;
}

void
sbox_swentry_set_name (SBoxSWEntry *swentry, const gchar *name)
{
  g_assert (swentry);
  g_assert (name);

  swentry->name = g_strdup (name);
}

void
sbox_swentry_set_description (SBoxSWEntry *swentry, const gchar *desc)
{
  g_assert (swentry);
  g_assert (desc);

  swentry->description = g_strdup (desc);
}

void
sbox_swentry_set_topic (SBoxSWEntry *swentry, const gchar *topic)
{
  g_assert (swentry);
  g_assert (topic);

  swentry->id = (gulong)jenkins_hash (topic);
  swentry->topic = g_strdup (topic);
}

void
sbox_swentry_set_payload_on (SBoxSWEntry *swentry, const gchar *payload_on)
{
  g_assert (swentry);
  g_assert (payload_on);

  swentry->payload_on = g_strdup (payload_on);
}

void
sbox_swentry_set_payload_off (SBoxSWEntry *swentry, const gchar *payload_off)
{
  g_assert (swentry);
  g_assert (payload_off);

  swentry->payload_off = g_strdup (payload_off);
}

const gchar *
sbox_swentry_get_name (SBoxSWEntry *swentry)
{
  g_assert (swentry);
  return swentry->name;
}

const gchar *
sbox_swentry_get_description (SBoxSWEntry *swentry)
{
  g_assert (swentry);
  return swentry->description;
}

const gchar *
sbox_swentry_get_topic (SBoxSWEntry *swentry)
{
  g_assert (swentry);
  return swentry->topic;
}

const gchar *
sbox_swentry_get_payload_on (SBoxSWEntry *swentry)
{
  g_assert (swentry);
  return swentry->payload_on;
}

const gchar *
sbox_swentry_get_payload_off (SBoxSWEntry *swentry)
{
  g_assert (swentry);
  return swentry->payload_off;
}
