/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sbox-connection.h"
#include "sbox-delegate.h"

#include <unistd.h>

/**
 * @brief Post new event
 * @param connection A pointer to the connection object
 * @param type The type of the new event to be posted
 */
static void post_connection_event (SBoxConnection *connection, ConnectionEventType type);
/**
 * @brief GSource prepare function
 */
static gboolean connection_source_prepare (GSource *source, gint *timeout);
/**
 * @brief GSource dispatch function
 */
static gboolean
connection_source_dispatch (GSource *source, GSourceFunc callback, gpointer _connection);
/**
 * @brief GSource callback function
 */
static gboolean connection_source_callback (gpointer _connection, gpointer _event);
/**
 * @brief GSource destroy notification callback function
 */
static void connection_source_destroy_notify (gpointer _connection);
/**
 * @brief Async queue destroy notification callback function
 */
static void connection_queue_destroy_notify (gpointer _connection);
/**
 * @brief Lunch mosquitto thread
 */
static void connection_mqtt_start (SBoxConnection *connection);
/**
 * @brief Stop mosquitto thread
 */
static void connection_mqtt_stop (SBoxConnection *connection);
/**
 * @brief MQTT Connect callback
 */
void mqtt_connect_callback (struct mosquitto *mosq, void *obj, int result);
/**
 * @brief MQTT Message callback
 */
void mqtt_message_callback (struct mosquitto *mosq,
                            void *obj,
                            const struct mosquitto_message *message);

/**
 * @brief GSourceFuncs vtable
 */
static GSourceFuncs connection_source_funcs = {
  connection_source_prepare,
  NULL,
  connection_source_dispatch,
  NULL,
  NULL,
  NULL,
};

static void
post_connection_event (SBoxConnection *connection, ConnectionEventType type)
{
  SBoxConnectionEvent *e = NULL;

  g_assert (connection);

  e = g_new0 (SBoxConnectionEvent, 1);
  e->type = type;
  g_async_queue_push (connection->queue, e);
}

static gboolean
connection_source_prepare (GSource *source, gint *timeout)
{
  SBoxConnection *connection = (SBoxConnection *)source;

  SBOX_UNUSED (timeout);

  return(g_async_queue_length (connection->queue) > 0);
}

static gboolean
connection_source_dispatch (GSource *source, GSourceFunc callback, gpointer _connection)
{
  SBoxConnection *connection = (SBoxConnection *)source;
  gpointer event = g_async_queue_try_pop (connection->queue);

  SBOX_UNUSED (callback);
  SBOX_UNUSED (_connection);

  if (event == NULL)
    return G_SOURCE_CONTINUE;

  return connection->callback (connection, event) == TRUE ? G_SOURCE_CONTINUE : G_SOURCE_REMOVE;
}

static gboolean
connection_source_callback (gpointer _connection, gpointer _event)
{
  SBoxConnection *connection = (SBoxConnection *)_connection;
  SBoxConnectionEvent *event = (SBoxConnectionEvent *)_event;

  g_assert (connection);
  g_assert (event);

  switch (event->type)
    {
    case CONN_EVENT_MQTT_START:
      connection_mqtt_start (connection);
      break;

    case CONN_EVENT_MQTT_STOP:
      connection_mqtt_stop (connection);
      break;

    default:
      break;
    }

  g_free (event);

  return TRUE;
}

static void
connection_source_destroy_notify (gpointer _connection)
{
  SBoxConnection *connection = (SBoxConnection *)_connection;

  g_assert (connection);
  g_debug ("Connection destroy notification");

  sbox_connection_unref (connection);
}

static void
connection_queue_destroy_notify (gpointer _connection)
{
  SBOX_UNUSED (_connection);
  g_debug ("Connection queue destroy notification");
}

static void
connect_subscribe_foreach_cb (gpointer database, gpointer swentry, gpointer arg1, gpointer arg2)
{
  SBoxConnection *connection = (SBoxConnection *)arg1;
  SBoxSWEntry *entry = (SBoxSWEntry *)swentry;

  SBOX_UNUSED (database);
  SBOX_UNUSED (arg2);

  mosquitto_subscribe (connection->mosq, NULL, sbox_swentry_get_topic (entry), 0);
}

void
mqtt_connect_callback (struct mosquitto *mosq, void *obj, int result)
{
  SBoxConnection *connection = (SBoxConnection *)obj;

  SBOX_UNUSED (mosq);

  g_info ("MQTT connect callback, result=%d", result);
  sbox_database_call_foreach_entry (
    connection->database, connect_subscribe_foreach_cb, connection, NULL, NULL);
}

static void
message_match_foreach_cb (gpointer database, gpointer swentry, gpointer arg1, gpointer arg2)
{
  const struct mosquitto_message *message = (const struct mosquitto_message *)arg1;
  SBoxSWEntry *entry = sbox_swentry_ref ((SBoxSWEntry *)swentry);
  bool match = false;

  SBOX_UNUSED (database);
  SBOX_UNUSED (arg2);

  mosquitto_topic_matches_sub (sbox_swentry_get_topic (entry), message->topic, &match);
  if (match)
    {
      gchar tmp[256] = { 0 };

      memcpy (tmp, message->payload, message->payloadlen);

      if (g_strcmp0 (tmp, sbox_swentry_get_payload_on (entry)) == 0)
        sbox_swentry_set_switch_state (entry, TRUE);
      else
        sbox_swentry_set_switch_state (entry, FALSE);

      {
        ExecutorActionPayload pld = { .entry = entry };

        g_debug ("Push new executor event for switch='%s' state=%d",
                 sbox_swentry_get_name (entry),
                 sbox_swentry_get_switch_state (entry));

        sbox_executor_push_event (
          sbox_delegate_get_executor (), EXECUTOR_SET_SWENTRY_SWITCH_STATE_CLOUD, pld);
      }
    }
}

void
mqtt_message_callback (struct mosquitto *mosq,
                       void *obj,
                       const struct mosquitto_message *message)
{
  SBoxConnection *connection = (SBoxConnection *)obj;
  struct mosquitto_message message_copy = { 0 };

  SBOX_UNUSED (mosq);
  g_assert (connection);
  mosquitto_message_copy (&message_copy, message);

  sbox_database_call_foreach_entry (
    connection->database, message_match_foreach_cb, &message_copy, NULL, NULL);

  mosquitto_message_free_contents (&message_copy);
}

static void *
mqtt_main_thread (void *arg)
{
  SBoxConnection *connection = sbox_connection_ref ((SBoxConnection *)arg);
  g_autofree gchar *host_addr = NULL;
  long host_port = -1;
  gint rc = 0;

  g_assert (connection);
  g_info ("MQTT Connection thread started");

  host_addr = sbox_database_get_connection_host_addr (connection->database, NULL);
  host_port = sbox_database_get_connection_host_port (connection->database, NULL);

  rc = mosquitto_connect (connection->mosq, host_addr, host_port, 60);
  while (!connection->thread_stop)
    {
      rc = mosquitto_loop (connection->mosq, -1, 1);
      if (!connection->thread_stop && rc)
        {
          g_info ("MQTT connection error");
          g_usleep (5000);
          mosquitto_reconnect (connection->mosq);
        }
    }

  sbox_connection_unref (connection);

  return NULL;
}

static void
connection_mqtt_start (SBoxConnection *connection)
{
  g_assert (connection);

  connection->thread_stop = FALSE;
  SBOX_UNUSED (pthread_create (&connection->thread, NULL, &mqtt_main_thread, connection));
}

static void
connection_mqtt_stop (SBoxConnection *connection)
{
  g_assert (connection);

  connection->thread_stop = TRUE;
  SBOX_UNUSED (mosquitto_loop_stop (connection->mosq, true));
  SBOX_UNUSED (mosquitto_disconnect (connection->mosq));
  SBOX_UNUSED (pthread_join (connection->thread, NULL));
  g_info ("MQTT Connection thread stopped");
}

SBoxConnection *
sbox_connection_new (SBoxDatabase *database)
{
  SBoxConnection *connection
    = (SBoxConnection *)g_source_new (&connection_source_funcs, sizeof(SBoxConnection));

  g_assert (connection);
  g_assert (database);

  g_ref_count_init (&connection->rc);
  connection->database = sbox_database_ref (database);
  connection->queue = g_async_queue_new_full (connection_queue_destroy_notify);
  connection->callback = connection_source_callback;
  connection->thread_stop = TRUE;

  mosquitto_lib_init ();

  connection->client_id = g_strdup_printf ("sbox_log_%d", getpid ());
  connection->mosq = mosquitto_new (connection->client_id, true, 0);
  if (connection->mosq == NULL)
    g_error ("Cannot create mosquitto client");
  g_assert (connection->mosq);

  mosquitto_user_data_set (connection->mosq, connection);
  mosquitto_connect_callback_set (connection->mosq, mqtt_connect_callback);
  mosquitto_message_callback_set (connection->mosq, mqtt_message_callback);

  g_source_set_callback (
    SBOX_EVENT_SOURCE (connection), NULL, connection, connection_source_destroy_notify);
  g_source_attach (SBOX_EVENT_SOURCE (connection), NULL);

  return connection;
}

SBoxConnection *
sbox_connection_ref (SBoxConnection *a)
{
  g_assert (a);
  g_ref_count_inc (&a->rc);
  return a;
}

void
sbox_connection_unref (SBoxConnection *a)
{
  g_assert (a);

  if (g_ref_count_dec (&a->rc) == TRUE)
    {
      if (a->database != NULL)
        sbox_database_unref (a->database);

      if (a->mosq != NULL)
        mosquitto_destroy (a->mosq);

      mosquitto_lib_cleanup ();
      g_free (a->client_id);

      g_async_queue_unref (a->queue);
      g_source_unref (SBOX_EVENT_SOURCE (a));
    }
}

void
sbox_connection_connect (SBoxConnection *connection)
{
  post_connection_event (connection, CONN_EVENT_MQTT_START);
}

void
sbox_connection_disconnect (SBoxConnection *connection)
{
  post_connection_event (connection, CONN_EVENT_MQTT_STOP);
}

void
sbox_connection_set_switch_state (SBoxConnection *connection,
                                  const gchar *topic,
                                  const gchar *state)
{
  g_assert (connection);
  g_assert (topic);
  g_assert (state);

  g_debug ("Report new switch state to MQTT topic='%s' state='%s'", topic, state);
  mosquitto_publish (connection->mosq, NULL, topic, strlen (state), state, 0, 0);
}

gboolean
sbox_connection_is_connected (SBoxConnection *connection)
{
  g_assert (connection);
  return !connection->thread_stop;
}
