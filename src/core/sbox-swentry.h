/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "sbox-types.h"

#include <glib.h>

G_BEGIN_DECLS

/**
 * @struct SBoxSEntry
 * @brief The SBoxSEntry opaque data structure
 */
typedef struct _SBoxSWEntry {
  gulong id;
  gchar *name;
  gchar *description;
  gchar *topic;
  gchar *payload_on;
  gchar *payload_off;
  gboolean state;
  gpointer widget;
  grefcount rc;   /**< Reference counter variable  */
} SBoxSWEntry;

#define SBOX_SWENTRY_TO_PTR(s) ((gpointer)(SBoxSWEntry *)(s))

/*
 * @brief Create a new swentry object
 * @return On success return a new SBoxSWEntry object otherwise return NULL
 */
SBoxSWEntry *sbox_swentry_new (void);

/**
 * @brief Aquire swentry object
 * @param swentry Pointer to the swentry object
 */
SBoxSWEntry *sbox_swentry_ref (SBoxSWEntry *swentry);

/**
 * @brief Release swentry object
 * @param swentry Pointer to the swentry object
 */
void sbox_swentry_unref (SBoxSWEntry *swentry);

/**
 * @brief Setter
 */
void sbox_swentry_set_id (SBoxSWEntry *swentry, gulong id);

/**
 * @brief Getter
 */
gulong sbox_swentry_get_id (SBoxSWEntry *swentry);

/**
 * @brief Setter
 */
void sbox_swentry_set_switch_state (SBoxSWEntry *swentry, gboolean state);

/**
 * @brief Getter
 */
gboolean sbox_swentry_get_switch_state (SBoxSWEntry *swentry);

/**
 * @brief Setter
 */
void sbox_swentry_set_widget (SBoxSWEntry *swentry, gpointer widget);

/**
 * @brief Getter
 */
gpointer sbox_swentry_get_widget (SBoxSWEntry *swentry);

/**
 * @brief Setter
 */
void sbox_swentry_set_name (SBoxSWEntry *swentry, const gchar *name);

/**
 * @brief Setter
 */
void sbox_swentry_set_description (SBoxSWEntry *swentry, const gchar *desc);

/**
 * @brief Setter
 */
void sbox_swentry_set_topic (SBoxSWEntry *swentry, const gchar *topic);

/**
 * @brief Setter
 */
void sbox_swentry_set_payload_on (SBoxSWEntry *swentry, const gchar *payload_on);

/**
 * @brief Setter
 */
void sbox_swentry_set_payload_off (SBoxSWEntry *swentry, const gchar *payload_off);

/**
 * @brief Getter
 */
const gchar *sbox_swentry_get_name (SBoxSWEntry *swentry);

/**
 * @brief Getter
 */
const gchar *sbox_swentry_get_description (SBoxSWEntry *swentry);

/**
 * @brief Getter
 */
const gchar *sbox_swentry_get_topic (SBoxSWEntry *swentry);

/**
 * @brief Getter
 */
const gchar *sbox_swentry_get_payload_on (SBoxSWEntry *swentry);

/**
 * @brief Getter
 */
const gchar *sbox_swentry_get_payload_off (SBoxSWEntry *swentry);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (SBoxSWEntry, sbox_swentry_unref);

G_END_DECLS
