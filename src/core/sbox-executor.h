/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "sbox-connection.h"
#include "sbox-database.h"
#include "sbox-swentry.h"
#include "sbox-types.h"

#include <glib.h>

G_BEGIN_DECLS

/**
 * @enum executor_event_data
 * @brief Executor event data type
 */
typedef enum _ExecutorEventType {
  EXECUTOR_CONNECT,
  EXECUTOR_DISCONNECT,
  EXECUTOR_ADD_SWENTRY,
  EXECUTOR_REM_SWENTRY,
  EXECUTOR_SET_SWENTRY_SWITCH_STATE,
  EXECUTOR_SET_SWENTRY_SWITCH_STATE_CLOUD,
  EXECUTOR_SET_SWENTRY_SWITCH_STATE_ALL,
} ExecutorEventType;

/**
 * @function SBoxExecutorCallback
 * @brief Custom callback used internally by SBoxExecutor as source callback
 */
typedef gboolean (*SBoxExecutorCallback)(gpointer _executor, gpointer _event);

/**
 * @struct ExecutorActionPayload
 * @brief The Action Event Payload
 */
typedef struct _ExecutorActionPayload {
  SBoxSWEntry *entry;
} ExecutorActionPayload;

/**
 * @struct SBoxExecutorEvent
 * @brief The Executor Event
 */
typedef struct _SBoxExecutorEvent {
  ExecutorEventType type;          /**< The event type the element holds */
  ExecutorActionPayload payload;   /**< The event payload the element holds */
} SBoxExecutorEvent;

/**
 * @struct SBoxExecutor
 * @brief The SBoxExecutor opaque data structure
 */
typedef struct _SBoxExecutor {
  GSource source;   /**< Event loop source */
  SBoxConnection *connection;
  SBoxDatabase *database;
  GAsyncQueue *queue;              /**< Async queue */
  SBoxExecutorCallback callback;   /**< Callback function */
  grefcount rc;
} SBoxExecutor;

/*
 * @brief Create a new executor object
 * @return On success return a new SBoxExecutor object otherwise return NULL
 */
SBoxExecutor *sbox_executor_new (SBoxConnection *connection, SBoxDatabase *database);

/**
 * @brief Aquire executor object
 */
SBoxExecutor *sbox_executor_ref (SBoxExecutor *executor);

/**
 * @brief Release executor object
 */
void sbox_executor_unref (SBoxExecutor *executor);

/**
 * @brief Push an executor event
 */
void sbox_executor_push_event (SBoxExecutor *executor,
                               ExecutorEventType type,
                               ExecutorActionPayload payload);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (SBoxExecutor, sbox_executor_unref);

G_END_DECLS
