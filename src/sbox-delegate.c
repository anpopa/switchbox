/*
 * Copyright 2021 Alin Popa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sbox-delegate.h"

/**
 * @struct SBoxDelegate
 * @brief Delegate object
 */
typedef struct _SBoxDelegate {
  SBoxConnection *connection;
  SBoxDatabase *database;
  SBoxExecutor *executor;
  gpointer main_window;
  gpointer settings_window;
} SBoxDelegate;

static SBoxDelegate *sbox_delegate_inst = NULL;

void
sbox_delegate_create (const gchar *dbpath, GError **error)
{
  sbox_delegate_inst = g_new0 (SBoxDelegate, 1);
  g_assert (sbox_delegate_inst);

  /* construct database noexept */
  sbox_delegate_inst->database = sbox_database_new (dbpath, error);

  /* construct connection noexept */
  sbox_delegate_inst->connection = sbox_connection_new (sbox_delegate_inst->database);

  /* construct executor */
  sbox_delegate_inst->executor
    = sbox_executor_new (sbox_delegate_inst->connection, sbox_delegate_inst->database);
}

void
sbox_delegate_destroy (void)
{
  g_assert (sbox_delegate_inst);

  if (sbox_delegate_inst->database != NULL)
    sbox_database_unref (sbox_delegate_inst->database);

  if (sbox_delegate_inst->connection != NULL)
    sbox_connection_unref (sbox_delegate_inst->connection);

  if (sbox_delegate_inst->executor != NULL)
    sbox_executor_unref (sbox_delegate_inst->executor);

  if (sbox_delegate_inst->main_window != NULL)
    g_object_unref (sbox_delegate_inst->main_window);

  if (sbox_delegate_inst->settings_window != NULL)
    g_object_unref (sbox_delegate_inst->settings_window);

  g_free (sbox_delegate_inst);
}

SBoxDatabase *
sbox_delegate_get_database (void)
{
  g_assert (sbox_delegate_inst);
  return sbox_database_ref (sbox_delegate_inst->database);
}

SBoxConnection *
sbox_delegate_get_connection (void)
{
  g_assert (sbox_delegate_inst);
  return sbox_connection_ref (sbox_delegate_inst->connection);
}

SBoxExecutor *
sbox_delegate_get_executor (void)
{
  g_assert (sbox_delegate_inst);
  return sbox_executor_ref (sbox_delegate_inst->executor);
}

void
sbox_delegate_set_main_window (gpointer win)
{
  g_assert (win);
  g_assert (sbox_delegate_inst);
  sbox_delegate_inst->main_window = g_object_ref (win);
}

gpointer
sbox_delegate_get_main_window (void)
{
  g_assert (sbox_delegate_inst);
  return sbox_delegate_inst->main_window;
}

void
sbox_delegate_set_settings_window (gpointer win)
{
  g_assert (win);
  g_assert (sbox_delegate_inst);
  sbox_delegate_inst->settings_window = g_object_ref (win);
}

gpointer
sbox_delegate_get_settings_window (void)
{
  g_assert (sbox_delegate_inst);
  return sbox_delegate_inst->settings_window;
}
